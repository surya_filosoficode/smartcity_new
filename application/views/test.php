<?php
    $url_main_app = base_url()."beranda";
    $url_sub_app = base_url()."beranda/sub-menu/";

    $main_controller = base_url()."smartcity";

    $path_img = "https://dashboard.malangkota.go.id/app_admin/assets/smart/img/icon/";


    $str_list_menu = ""; 
    // $str_list_content = ""; 
    
    if(isset($list_data)){
        if($list_data){

            print_r("<pre>");
            print_r($list_data);

            foreach ($list_data as $key => $value) {
                $title_menu = $value["title"];
                    $title_name = $title_menu->nama_mn_smart_ct;
                    $title_kd = $title_menu->kd_mn_smart_ct;
                $item = $value["item"];

                if($title_kd != "DEFAULT"){
                    $str_list_menu .= "<br><br><h3 style=\"font-family: Arial, Helvetica, sans-serif;\">".$title_name."</h3><br>";
                }

                $str_list_menu .= "<div class=\"row\">";
                foreach ($item as $key_i => $value_i) {
                    $id_mn_smart = $value_i->id_mn_smart;
                    $mn_smart_id = $value_i->mn_smart_id;
                    $parent_mn_smart = $value_i->parent_mn_smart;
                    $nama_mn_smart = $value_i->nama_mn_smart;
                    $ket_mn_smart = $value_i->ket_mn_smart;
                    $sts_mn_smart = $value_i->sts_mn_smart;
                    $slug_mn_smart = $value_i->slug_mn_smart;
                    $link_mn_smart = $value_i->link_mn_smart;
                    $icon_mn_smart = $value_i->icon_mn_smart;
                    $seo_mn_smart = $value_i->seo_mn_smart;
                    $sts_active = $value_i->sts_active;

                    if($sts_active == "1"){
                        if($sts_mn_smart == "0"){
                            $str_list_menu .= "
                            <div class=\"col-sm-4\" style=\"margin-top:30px;\">
                                <a href=\"".$url_sub_app.hash("sha256", $id_mn_smart)."/?slug=".$slug_mn_smart."\" class=\"banner-btn\">
                                <img src=\"".$path_img.$icon_mn_smart."\" style=\"height: 80px; width: 80px\" class=\"main-logo\" width=\"\" alt=\"Awesome Image\" />
                                    <span>".$nama_mn_smart."</span>
                                </a>
                            </div>";
                        }else{
                            $str_list_menu .= "
                            <div class=\"col-sm-4\" style=\"margin-top:30px;\">
                                <a href=\"".$link_mn_smart."\" class=\"banner-btn\" target=\"_blank\">
                                <img src=\"".$path_img.$icon_mn_smart."\" style=\"height: 80px; width: 80px\" class=\"main-logo\" width=\"\" alt=\"Awesome Image\" />
                                    <span>".$nama_mn_smart."</span>
                                </a>
                            </div>";
                        }
                    }
                    
                }
                
                
                $str_list_menu .= "</div>";
            }
            // foreach ($list_data as $key => $value) {

            // }
        }
    }

?>
<!DOCTYPE html>
<!-- edited 15 06 2021 -->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>5AM - Smart City</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Malang Smart City, Malang Kota Cerdas" />
    <meta name="keywords" content="Malang Smart City, Malang Kota Cerdas, smart city, kota malang, malang" />

    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css"> -->


    <!-- <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png"> -->


    <meta name="msapplication-TileColor" content="#ffffff">
    <!-- <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png"> -->
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <!-- <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css"> -->

   <style type="text/css">
    .button {
      padding: 10px 20px;
      font-size: 24px;
      text-align: center;
      cursor: pointer;
      outline: none;
      color: #fff;
      background-color: #3367D6;
      border: none;
      border-radius: 15px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgb(52 152 219 / 20%);
  /*    box-shadow: 3px 3px #4ECCF1;*/
    }

    .button:hover {background-color:
    #9ab0b8;}

    .button:active {
      background-color: #AFEEEE;
      box-shadow: 0 5px #666;
      transform: translateY(4px);
    }
  </style>

</head>

<body>
    <div class="page-wrapper">
        <!-- <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="#">
                            <img src="<?= base_url(); ?>assets/images/logo_new.png" alt="Awesome Image" />
                        </a>

                    </div>
                    <div class="main-navigation">
                        <ul class="one-page-scroll-menu navigation-box">
                            <li class=" current scrollToLink">
                                <a href="#banner"></a>
                            </li>

                        </ul>
                    </div>
                    <div class="right-side-box">
                    </div>
                </div>
            </nav>
        </header> -->

        <section class="banner-style-one" id="banner">
            
            <br>  <br>  <br>  <br>  <br>
            <!-- <img src="<?= base_url(); ?>assets6/images/headernew2.png" class="banner-mock" style="width:70%; height:auto;" alt="Awesome Image" /> -->
            <div class="container">
                <!-- <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="content-block">
                            <br><br><br><br><br>
                            <p><a href="<?= base_url(); ?>tentang" class="button">Tentang Kota Malang Smartcity</a></p>
                                <br><br>
                            <div class="row no-gutter" style="text-align: center;"> -->
                                <?=$str_list_menu?>
                            <!-- </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </section>





                    <img src="<?= base_url(); ?>assets/images/iconic_mlg_nonbg.png" width="100%">

            <hr class="rgba-white-light" style="margin: 0 15%;">
       <div class=" d-flex text-center justify-content-center mb-md-0 mb-4">
            </div>

                <div class="col-md-12">

                       <div class="footer-widget">

                                <div class="social-block text-center ">
                                      <a href="https://www.facebook.com/malangkota.go.id" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/facebook.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-twitter --></a>
                                      <a href="https://twitter.com/PemkotMalang" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/twitter.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-twitter --></a>
                                      <a href="https://www.instagram.com/pemkotmalang/" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/instagram.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-instagram --></a>
                                      <a href="https://www.youtube.com/user/mediacentermalang" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/youtube.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-instagram --></a>
                                </div><!-- /.social-block -->
                            </div><!-- /.footer-widget -->
                </div>

        </div>
        <div class="bottom-footer text-center">
            <div class="container">
                <p>&copy; copyright 2021 <a href="#" style="color: blue">Pemerintah Kota Malang</a></p>
            </div><!-- /.container -->
        </div><!-- /.bottom-footer -->
    </div><!-- /.page-wrapper -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i><img src="<?= base_url(); ?>assets6/images/icon_portal/up-chevron.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i></a>
    <!-- /.scroll-to-top -->
    <!-- <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script> -->
</body>
</html>


<!-- ===================PORTAL MALANGKOTA ================================-->
<!-- ========================= DONAT ================================-->
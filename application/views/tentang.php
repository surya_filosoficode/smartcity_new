<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>Tentang 5AM</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Malang Smart City, Malang Kota Cerdas" />
    <meta name="keywords" content="Malang Smart City, Malang Kota Cerdas, smart city, kota malang, malang" />

    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">


    <link rel="manifest" href="<?= base_url(); ?>assets6/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">
</head>

<body>

    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="<?= base_url(); ?>">
                            <img src="<?= base_url(); ?>assets6/images/logo-1-1.png" alt="Awesome Image" />
                        </a>
                        <button class="menu-toggler" data-target=".header-one .main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class="one-page-scroll-menu navigation-box">
                            <li></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <a href="<?= base_url(); ?>" class="header-btn">Beranda</a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

        <section class="services-style-one" id="service">
            <div class="container"><br><br><br>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="image-block">
                            <img src="<?= base_url(); ?>assets6/images/gambar1.png" alt="Awesome Image" />
                        </div><!-- /.image-block -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="content-block">
                            <div class="block-title ">

                                <h2>Profil Kota Malang <br> Smart City</h2>
                            </div><!-- /.block-title -->
                            <p><b>Smart City </b>adalah konsep Pengembangan dan Pengelolaan kota dengan pemanfaatan Teknologi Informasi dan Komunikasi (TIK) untuk menghubungkan, memonitor dan mengendalikan berbagai sumber daya yang ada di dalam kota dengan lebih efektif dan efisien untuk memaksimalkan pelayanan kepada warganya serta mendukung pembangunan yang berkelanjutan. <br><b> 6 Dimensi Smart City:</b></p>
                            <ul class="feature-lists">
                                <li><i class="fa fa-check"></i> Smart Governance</li>
                                <li><i class="fa fa-check"></i> Smart Economy</li>
                                <li><i class="fa fa-check"></i> Smart Living</li>
                                <li><i class="fa fa-check"></i> Smart Environment</li>
                                <li><i class="fa fa-check"></i> Smart Society</li>
                                <li><i class="fa fa-check"></i> Smart Branding</li>
                            </ul><!-- /.feature-lists -->

                        </div><!-- /.content-block -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.services-style-one -->

        <section class="feature-style-one">
            <div class="container">
                <hr class="style-one" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="content-block">
                            <div class="block-title ">
                                <img src="<?= base_url(); ?>assets6/images/round-circle-1-3.png" alt="Awesome Image" class="wow rotateIn" data-wow-duration="1000ms" />
                                <h2>Visi Misi<br> Kota Malang Smart City</h2>
                            </div><!-- /.block-title -->
                            <h3>Visi</h3>
                            <p style="text-align: left;">“Mewujudkan Kota Malang sebagai <br>Kota Cerdas yang Inovatif”</p>
                              <h3>Misi</h3>
                            <ul class="feature-lists">
                                <li><i class="fa fa-check"></i> Menjamin Akses dan Kualitas Pendidikan, Kesehatan, dan Layanan Dasar Lainnya Bagi Semua Warga <br> <!-- <u>Dimensi Smart city: Smart Living, Smart Environment</u> --></li>

                                <li><i class="fa fa-check"></i> Mewujudkan Kota Produktif dan Berdaya Saing berbasis Ekonomi Kreatif, Keberlanjutan dan Keterpaduan <br> <!-- <u>Dimensi Smart city: Smart Economy, Smart Branding</u> --></li>

                               <li><i class="fa fa-check"></i> Mewujudkan Kota yang Rukun dan Toleran berasaskan Keberagaman dan Keberpihakan terhadap Masyarakat Rentan dan Gender<br><!--  <u>Dimensi Smart city: Smart Living, Smart Society</u> --></li>

                               <li><i class="fa fa-check"></i> Memastikan Kepuasan Masyarakat atas Layanan Pemerintah yang Tertib Hukum, Profesional dan Akuntabel <br> <!-- <u>Dimensi Smart city: Smart Governance</u> --></li>

                               <li><i class="fa fa-check"></i>Mendayagunakan sumber daya secara efektif dan efisien <br><!--  <u>Dimensi Smart city: Semua Dimensi</u> --></li>


                               <li><i class="fa fa-check"></i> Mencapai good governance, clean environment, comfort mobility, comfort living, berdaya saing, dan masyarakat kreatif<br> <!-- <u>Dimensi Smart city: Semua Dimensi</u> --></li>

                            </ul><!-- /.feature-lists -->

                        </div><!-- /.content-block -->
                    </div><!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <div class="image-block">
                            <img src="<?= base_url(); ?>assets6/images/gambar2.png" alt="Awesome Image" />
                        </div><!-- /.image-block -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.feature-style-one -->

        <section class="testimonials-style-one">
            <img src="<?= base_url(); ?>assets6/images/map-1-1.png" alt="Awesome Image" class="map-img" />
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 d-flex">
                        <div class="my-auto">
                            <!-- <img src="images/testi-1-1.png" alt="Awesome Image" class="testi-img" /> -->
                            <div id="testimonials-slider-pager">
                                <div class="testimonials-slider-pager-one">
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item active" data-slide-index="0"><img src="<?= base_url(); ?>assets6/images/smg.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="1"><img src="<?= base_url(); ?>assets6/images/smg2.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="2"><img src="<?= base_url(); ?>assets6/images/smg3.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="3"><img src="<?= base_url(); ?>assets6/images/smg4.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="4"><img src="<?= base_url(); ?>assets6/images/smg5.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="5"><img src="<?= base_url(); ?>assets6/images/smg6.png" alt="Awesome Image" /></a>
                                </div><!-- /.testimonials-slider-pager-one -->
                                <div class="testimonials-slider-pager-two">
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item active" data-slide-index="0"><img src="<?= base_url(); ?>assets6/images/smg.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="1"><img src="<?= base_url(); ?>assets6/images/smg2.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="2"><img src="<?= base_url(); ?>assets6/images/smg3.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="3"><img src="<?= base_url(); ?>assets6/images/smg4.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="4"><img src="<?= base_url(); ?>assets6/images/smg5.png" alt="Awesome Image" /></a>
                                    <a href="<?= base_url(); ?>assets6/#" class="pager-item" data-slide-index="5"><img src="<?= base_url(); ?>assets6/images/smg6.png" alt="Awesome Image" /></a>
                                </div><!-- /.testimonials-slider-pager-two -->
                            </div><!-- /#testimonials-slider-pager -->
                        </div><!-- /.my-auto -->
                    </div><!-- /.col-lg-6 -->

                    <div class="col-lg-6">
                        <div class="block-title ">
                            <img src="<?= base_url(); ?>assets6/images/round-circle-1-6.png" alt="Awesome Image" class="wow rotateIn" data-wow-duration="1000ms" />
                            <h2>Dimensi<br> Smart City</h2>
                        </div><!-- /.block-title -->
                        <ul class="slider testimonials-slider">
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p> <b>Smart Governance</b>
                                        adalah sebuah tata kelola pemerintahan yang cerdas. Yang bertujuan untuk mewujudkan tata kelola dan tata pamong pemerintahan ekfektif, efisien, komunikatif, dan terus melakukan peningkatan kinerja birokrasi melalui inovasi dan adopsi teknologi yang terpadu.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p><b>Smart Environment</b>
                                    adalah pengelolaan lingkungan yang pintar, dengan memperhatikan lingkungan hidup dalam pembangunan kota yang sama besarnya dengan perhatian yang diberikan terhadap pembangunan infrastruktur fisik maupun pembangunan bagi sarana dan prasarana bagi warga.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p><b>Smart Society</b>
                                    adalah mewujudkan ekosistem sosio-teknis masyarakat yang humanis dan dinamis, baik fisik maupun virtual untuk terciptanya masyarakat yang produktif, komunikatif, dan interaktif dengan digital literacy yang tinggi.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p><b>Smart Living</b>
                                    untuk mewujudkan lingkungan tempat tinggal yang layak tinggal, nyaman, dan efisien.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p><b>Smart Branding</b>
                                    adalah inovasi dalam memasarkan daerahnya sehingga mampu meningkatkan daya saing daerah dengan mengembankan tiga elemen, yaitu pariwisata, bisnis, dan wajah kota.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                            <li class="slide-item">
                                <div class="single-testi-one">
                                    <p><b>Smart Economy</b>
                                    merupakan tata kelola perekonomian yang pintar, yang dimaksudkan untuk mewujudkan ekosistem perekonomian di daerah yang mampu memenuhi tantangan di era informasi yang disruptif dan menuntut tingkat adaptasi yang cepat seperti saat ini.</p>

                                </div><!-- /.single-testi-one -->
                            </li>
                        </ul>
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.testimonials-style-one -->

         <section class="faq-style-one">
            <div class="container">
                <div class="block-title text-center">
                    <img src="<?= base_url(); ?>assets6/images/round-circle-1-7.png" alt="Awesome Image" class="wow rotateIn" data-wow-duration="1000ms" />
                    <h2>Dimensi<br> Smart City</h2>
                </div><!-- /.block-title -->
                <div class="accrodion-grp" data-grp-name="faq-accrodion">
                    <div class="accrodion active">
                        <div class="accrodion-title">
                            <h4>Smart Governance</h4>
                        </div>

                        <div class="accrodion-content">
                            <div class="inner">
                                <b>Sub Dimensi</b><br>
                                    <li>Pelayanan Publik (Public Service)</li>
                                    <li>Manajemen Birokrasi yang Efisien (Bureaucracy)</li>
                                    <li>Efisiensi Kebijakan Publik (Public Policy)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                                               <table>
                                  <tr>
                                    <th  colspan="3" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>Dashboard (Diskominfo)</td>
                                    <td>IZOL (DisnakerPMPTSP)</td>
                                    <td>SIBANSOS (Dinsos)</td>
                                  </tr>
                                  <tr>
                                    <td>E-Pokir (Dprd)</td>
                                    <td>REDIVET (Dispangtan)</td>
                                    <td>SIMPEL (Diskopindag)</td>
                                  </tr>
                                  <tr>
                                    <td>SURADI (Diskominfo)</td>
                                    <td>SIPRETI (Bkpsdm)</td>
                                    <td>SIMAS (Bkpsdm)</td>
                                  </tr>
                                  <tr>
                                    <td>E-Kinerja (Bkpsdm)</td>
                                    <td>Helen Bennett</td>
                                    <td>JDIH (Bag. hukum)</td>
                                  </tr>
                                  <tr>
                                    <td>SIPALANG (Blp)</td>
                                    <td>SIMPKK (TP PKK Kota Malang)</td>
                                    <td>E-TLHP (Inspektorat)</td>
                                  </tr>
                                  <tr>
                                    <td>SIMBADA (Bkad)</td>
                                    <td>SAMPADE (Bapenda)</td>
                                    <td>Aplikasi Pajak Daerah (Bapenda)</td>
                                  </tr>
                                   <tr>
                                    <td>Makotamail</td>
                                    <td></td>
                                    <td></td>
                                  </tr>
                                </table>
                            </div><!-- /.inner -->
                        </div>
                    </div>
                    <div class="accrodion ">
                        <div class="accrodion-title">
                            <h4>Smart Economy</h4>
                        </div>
                        <div class="accrodion-content">
                            <div class="inner">
                                 <b>Sub Dimensi</b><br>
                                    <li>Ekosistem industri yang berdaya saing (Industry)</li>
                                    <li>Kesejahteraan Rakyat (Welfare)</li>
                                    <li>Ekosistem Transaksi Keuangan (Transaction)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                 <table>
                                  <tr>
                                    <th  colspan="1" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>Sistem Informasi Harga Pangan / Sembako malang (Diskoperindag)</td>

                                  </tr>
                                  <tr>
                                    <td>Pasar Rakyat Malang (Diskoperindag)</td>

                                  </tr>
                                  <tr>
                                    <td>Pasar MBOIS</td>

                                  </tr>
                                  <tr>
                                    <td>Cari Jagoan UMKM</td>

                                  </tr>

                                </table>
                            </div><!-- /.inner -->
                        </div>
                    </div>
                    <div class="accrodion">
                        <div class="accrodion-title">
                            <h4>Smart Living</h4>
                        </div>
                        <div class="accrodion-content">
                            <div class="inner">
                                 <b>Sub Dimensi</b><br>
                                    <li>Ekosistem industri yang berdaya saing (Industry)</li>
                                    <li>Prasarana Kesehatan (Health)</li>
                                    <li>Ketersediaan Sarana Transportasi (Mobility)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                 <table>
                                  <tr>
                                    <th  colspan="3" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>NGALAM 112 (Diskominfo)</td>
                                     <td>SAMBAT (Diskominfo)</td>
                                      <td>SIPETARUNG (Bappeda)</td>

                                  </tr>
                                  <tr>
                                    <td>CCTV (Diskominfo)</td>
                                     <td>Website Covid-19 (Dinkes)</td>
                                      <td>RUVID (UB)</td>

                                  </tr>
                                  <tr>
                                    <td>Jogo Malang (Polresta)</td>
                                    <td>E-Parking</td>
                                    <td>E-SPM (Dinkes, Dinsos, BPJS)</td>

                                  </tr>
                                  <tr>
                                    <td>SIMONPARMA (Dishub)</td>
                                    <td>SISPARMA (Dishub)</td>
                                    <td></td>

                                  </tr>

                                </table>
                            </div><!-- /.inner -->

                        </div>
                    </div>
                    <div class="accrodion">
                        <div class="accrodion-title">
                            <h4>Smart Evironment</h4>
                        </div>
                        <div class="accrodion-content">
                            <div class="inner">
                                 <b>Sub Dimensi</b><br>
                                    <li> Proteksi Lingkungan (Protection)</li>
                                    <li>Tata Kelola Sampah dan Limbah (Waste)</li>
                                    <li>Tata Kelola Energi Yang Bertanggungjawab (Energy)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                 <table>
                                  <tr>
                                    <th  colspan="3" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>SIPALDI (Dpuprpkp)</td>
                                     <td>PDAM</td>
                                      <td>Kipop (Aplikasi KTP Pohon) (Disperkim)</td>

                                  </tr>
                                  <tr>
                                    <td>Free Wifi di 26 Titik Layanan Publik</td>
                                     <td>42 Hotspot Kantor Layanan Publik</td>
                                      <td>Free Wifi di 551 spot seluruh RW se-Kota Malang</td>

                                  </tr>


                                </table>
                            </div><!-- /.inner -->

                        </div>
                    </div>
                    <div class="accrodion">
                        <div class="accrodion-title">
                            <h4>Smart Society</h4>
                        </div>
                        <div class="accrodion-content">
                            <div class="inner">
                                 <b>Sub Dimensi</b><br>
                                    <li> Interaksi Masyarakat Yang Efisien (Community)</li>
                                    <li>Ekosistem Belajar Yang Efisien (Learning)</li>
                                    <li>Sistem Keamanan Masyarakat (Security)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                 <table>
                                  <tr>
                                    <th  colspan="1" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>Aplikasi Malang Cilin Digital Access (Disspusipda)</td>


                                  </tr>
                                  <tr>
                                    <td>Terbentuknya berbagai komunitas digital di Malang</td>


                                  </tr>
                                   <tr>
                                    <td>Program co-working space di 5 Kecamatan</td>


                                  </tr>


                                </table>
                            </div><!-- /.inner -->

                        </div>
                    </div>
                    <div class="accrodion">
                        <div class="accrodion-title">
                            <h4>Smart Branding</h4>
                        </div>
                        <div class="accrodion-content">
                            <div class="inner">
                                 <b>Sub Dimensi</b><br>
                                    <li> Membangun dan Memasarkan Ekosistem Pariwisata (Tourism Branding)</li>
                                    <li>Membangun Platform dan Memasarkan Ekosistem Bisnis Daerah (Business Branding)</li>
                                    <li>Membangun dan Memasarkan Wajah Kota (City Appearance Branding)</li><br>

                                <style type="text/css">table {
                                  font-family: arial, sans-serif;
                                  border-collapse: collapse;
                                  width: 100%;
                                }

                                td, th {
                                  border: 1px solid #dddddd;
                                  text-align: left;
                                  padding: 8px;
                                }</style>
                                 <table>
                                  <tr>
                                    <th  colspan="1" style="text-align: center;">Aplikasi</th>

                                  </tr>
                                  <tr>
                                    <td>Malang Meyapa (Disporapar)</td>


                                  </tr>
                                  <tr>
                                    <td>Aplikasi KER (Diskominfo)</td>


                                  </tr>
                                   <tr>
                                    <td>Festival Mbois</td>


                                  </tr>


                                </table>
                            </div><!-- /.inner -->

                        </div>
                    </div>
                </div>
            </div><!-- /.container -->
        </section><!-- /.faq-style-one -->


        <div class="bottom-footer text-center">
            <div class="container">
                <p>&copy; copyright 2021 by <a href="<?= base_url(); ?>assets6/#">Dinas Komunikasi dan Informatika <br> Pemerintah Kota Malang</a></p>
            </div><!-- /.container -->
        </div><!-- /.bottom-footer -->
    </div><!-- /.page-wrapper -->
    <a href="<?= base_url(); ?>assets6/#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-long-arrow-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>

</html>

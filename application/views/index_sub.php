
<?php
    $url_main_app = base_url()."beranda";
    $url_sub_app = base_url()."beranda/sub-menu/";

    $main_controller = base_url()."smartcity";

    $path_img = "https://dashboard.malangkota.go.id/app_admin/assets/smart/img/icon/";


    $str_list_menu = ""; 
    // $str_list_content = ""; 
    
    if(isset($list_data)){
        if($list_data){

            // print_r("<pre>");
            // print_r($list_data);

            foreach ($list_data as $key => $value) {
                $title_menu = $value["title"];
                    $title_name = $title_menu->nama_mn_smart_ct;
                    $title_kd = $title_menu->kd_mn_smart_ct;
                $item = $value["item"];

                if($title_kd != "DEFAULT" && $item){
                    $str_list_menu .= "<br><br><h3 style=\"font-family: Arial, Helvetica, sans-serif;\">".$title_name."</h3><br>";
                }

                $str_list_menu .= "<div class=\"row\">";
                foreach ($item as $key_i => $value_i) {
                    $id_mn_smart = $value_i->id_mn_smart;
                    $mn_smart_id = $value_i->mn_smart_id;
                    $parent_mn_smart = $value_i->parent_mn_smart;
                    $nama_mn_smart = $value_i->nama_mn_smart;
                    $ket_mn_smart = $value_i->ket_mn_smart;
                    $sts_mn_smart = $value_i->sts_mn_smart;
                    $slug_mn_smart = $value_i->slug_mn_smart;
                    $link_mn_smart = $value_i->link_mn_smart;
                    $icon_mn_smart = $value_i->icon_mn_smart;
                    $seo_mn_smart = $value_i->seo_mn_smart;
                    $sts_active = $value_i->sts_active;

                    if($sts_active == "1"){
                        if($sts_mn_smart == "0"){
                            $str_list_menu .= "
                            <div class=\"tour-category-one__col wow fadeInUp \" data-wow-duration=\"1500ms\" data-wow-delay=\"000ms\">
                                <div class=\"tour-category-one__single rounded-lg\">
                                    <a href=\"".$url_sub_app.hash("sha256", $id_mn_smart)."/?slug=".$slug_mn_smart."\">
                                        <img src=\"".$path_img.$icon_mn_smart."\" style=\"height: 120px; width: 120px\" class=\"main-logo\"  width=\"\" alt=\"Awesome Image\" />
                                        <!-- <i class=\"tripo-icon-message\"></i> -->
                                        <h3>".$nama_mn_smart."
                                    </a>
                                    </h3>
                                </div>
                            </div>";
                        }else{
                            $str_list_menu .= "
                            <div class=\"tour-category-one__col wow fadeInUp \" data-wow-duration=\"1500ms\" data-wow-delay=\"000ms\">
                                <div class=\"tour-category-one__single rounded-lg\">
                                    <a href=\"".$link_mn_smart."\" target=\"_blank\">
                                        <img src=\"".$path_img.$icon_mn_smart."\" style=\"height: 120px; width: 120px\" class=\"main-logo\"  width=\"\" alt=\"Awesome Image\" />
                                        <!-- <i class=\"tripo-icon-message\"></i> -->
                                        <h3>".$nama_mn_smart."
                                    </a>
                                    </h3>
                                </div>
                            </div>";
                        }
                    }
                }
                
                $str_list_menu .= "</div>";
            }
            // foreach ($list_data as $key => $value) {

            // }
        }
    }


    $title_parent = "";
    if(isset($parent)){
        if($parent){
            $title_parent = $parent["nama_mn_smart"];
        }
    }

    $str_bchum = "<nav>
                    <ol class=\"breadcrumb-list\">
                      <li class=\"breadcrumb-item\"><a href=\"".$url_main_app."\">Beranda</a></li>";
    if(isset($patern)){
      if($patern){
        foreach ($patern as $key => $value) {
          // print_r($value);
          $id_mn_smart_patern = $value["id_mn_smart"];
          $nama_mn_smart_patern = str_replace("<br>", " ", $value["nama_mn_smart"]);
          $slug_mn_smart_patern = $value["slug_mn_smart"];

          $str_bchum .= "<li class=\"breadcrumb-item active\"><a href=\"".$url_sub_app.hash("sha256", $id_mn_smart_patern)."/?slug=".$slug_mn_smart_patern."\">".$nama_mn_smart_patern."</a></li>";          
        }
      }
    }

    if(isset($parent)){
      if($parent){
        // print_r($parent);
          $id_mn_smart_parent = $parent["id_mn_smart"];
          $nama_mn_smart_parent = str_replace("<br>", " ", $parent["nama_mn_smart"]);
          $slug_mn_smart_parent = $parent["slug_mn_smart"];
        $str_bchum .= "<li class=\"breadcrumb-item active\"><a href=\"javascript:void(0)\">".$nama_mn_smart_parent."</a></li>";
      }
    }

    $str_bchum .= "</ol>
              </nav>";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>5AM Smart Governance</title>
    <!-- development progress -->
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <!--  -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/nouislider.pips.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/responsive.css') ?>" rel="stylesheet">

    <style>
        img {
            width: 100%;
            height: auto;
        }
        .breadcrumb-list {
            display: flex;
            flex-wrap: wrap;
            padding: 0;
            margin: 1rem 0 0 0;
            list-style: none;
            li {
            font-size: 0.85rem;
            letter-spacing: 0.125rem;
            text-transform: uppercase;
            }
        }

        .breadcrumb-item {
            &.active {
            color: $black;
            }
            + .breadcrumb-item {
            &::before {
                content: '/';
                display: inline-block;
                padding: 0 0.5rem;
                color: $lightergray;
            }
            }
        }
    </style>
</head>
<body>
   <div class="preloader">
   </div>
   <!-- /.preloader -->
   <div class="page-wrapper">
      <header class="site-header header-one">
         <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
            <div class="container clearfix">
               <!-- Brand and toggle get grouped for better mobile display -->
               <div class="logo-box clearfix">
                  <a class="navbar-brand" href="https://malangkota.go.id/"><br>
                  <img src="<?= base_url(); ?>assets6/images/smart_logo.png" alt="Awesome Image" />
                  </a>
               </div>
               <!-- /.logo-box -->
               <!-- Collect the nav links, forms, and other content for toggling -->
               <div class="main-navigation">
                  <li style="color: transparent;"></li>
               </div>
               <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
         </nav>
      </header>
      <!-- /.site-header -->
      <!-- ISI CONTENT -->
      <section class="tour-two tour-list banner-style-one">
         <div class="container">
            <div class="tour-sorter-one">
                <?=$str_bchum?>
            </div>
            
            <!-- /.tour-sorter-one -->
            <center>
               <h1 style=" font-family: Arial, Helvetica, sans-serif;"><?=$nama_mn_smart_parent?></h1>
               
               <!-- /.tour-sorter-one__right -->
               <br><br>
            </center>
            <!-- bagian dan kantor -->
            <?=$str_list_menu?>
         </div>
         <!-- /.container -->
      </section>
      <!-- /.gallery-one -->
      <!-- ================================================================================ -->
      <footer class="site-footer">
      <img src="<?= base_url(); ?>assets/images/wave3.png" style="width: 100%" class="banner-satu" width="" alt="Awesome Image" />
      <!-- /.site-footer__bg -->
      <!-- /.site-footer__bg -->
      <div class="site-footer__bottom">
         <div class="container">
            <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
               Pemerintah Kota Malang</a>
            </p>
            <div class="site-footer__social">
               <a href="https://www.facebook.com/malangkota.go.id"><i class="fa fa-facebook-square"></i></a>
               <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
               <a href="https://www.instagram.com/pemkotmalang/"><i class="fa fa-instagram"></i></a>
            </div>
            <!-- /.site-footer__social -->
         </div>
         <!-- /.container -->
      </div>
      <!-- /.site-footer__bottom -->
   </div>
   <!-- /.page-wrapper -->
   <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
   <div class="side-menu__block">
      <div class="side-menu__block-overlay custom-cursor__overlay">
         <div class="cursor"></div>
         <div class="cursor-follower"></div>
      </div>
      <!-- /.side-menu__block-overlay -->
      <div class="side-menu__block-inner ">
         <div class="side-menu__top justify-content-end">
            <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="<?= base_url(); ?>assets/images/shapes/close-1-1.png" alt=""></a>
         </div>
         <!-- /.side-menu__top -->
         <nav class="mobile-nav__container">
            <!-- content is loading via js -->
         </nav>
         <div class="side-menu__sep"></div>
         <!-- /.side-menu__sep -->
         <div class="side-menu__content">
            <p>@ All copyright 2021, <a href="#">Dinas Komunikasi dan Informatika <br>
               Pemerintah Kota Malang</a>
            </p>
            <div class="side-menu__social">
               <a href="https://www.facebook.com/malangkota.go.id">
                  <i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square -->
               </a>
               <a href="https://twitter.com/PemkotMalang">
                  <i class="fab fa-twitter"></i><!-- /.fab fa-twitter -->
               </a>
               <a href="https://www.instagram.com/pemkotmalang/">
                  <i class="fab fa-instagram"></i><!-- /.fab fa-instagram -->
               </a>
            </div>
         </div>
         <!-- /.side-menu__content -->
      </div>
      <!-- /.side-menu__block-inner -->
   </div>
   <!-- /.side-menu__block -->
   <div class="search-popup">
      <div class="search-popup__overlay custom-cursor__overlay">
         <div class="cursor"></div>
         <div class="cursor-follower"></div>
      </div>
      <!-- /.search-popup__overlay -->
      <div class="search-popup__inner">
         <form action="#" class="search-popup__form">
            <input type="text" name="search" placeholder="Type here to Search....">
            <button type="submit"><i class="fa fa-search"></i></button>
         </form>
      </div>
      <!-- /.search-popup__inner -->
   </div>
   <!-- /.search-popup -->
   <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
   <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
   <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
   <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>
</html>

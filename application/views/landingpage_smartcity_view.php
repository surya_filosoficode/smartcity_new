<!DOCTYPE html>
<!-- edited 15 06 2021 -->
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <title>5AM - Smart City</title>
    <!-- mobile responsive meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/animate.min.css">


    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>assets/images/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>assets/images/favicons/favicon-16x16.png">


    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url(); ?>assets6/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- main template stylesheet -->
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>assets6/css/responsive.css">

   <style type="text/css">
    .button {
      padding: 10px 20px;
      font-size: 24px;
      text-align: center;
      cursor: pointer;
      outline: none;
      color: #fff;
      background-color: #3367D6;
      border: none;
      border-radius: 15px;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgb(52 152 219 / 20%);
  /*    box-shadow: 3px 3px #4ECCF1;*/
    }

    .button:hover {background-color:
    #9ab0b8;}

    .button:active {
      background-color: #AFEEEE;
      box-shadow: 0 5px #666;
      transform: translateY(4px);
    }
  </style>

</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="#">
                            <img src="<?= base_url(); ?>assets/images/logo_new.png" alt="Awesome Image" />
                        </a>

                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class="one-page-scroll-menu navigation-box">
                            <li class=" current scrollToLink">
                                <a href="#banner"></a>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.site-header -->

        <section class="banner-style-one" id="banner">
            <span class="bubble-1"></span>
            <span class="bubble-2"></span>
            <span class="bubble-3"></span>
            <span class="bubble-4"></span>
            <span class="bubble-5"></span>
            <span class="bubble-6"></span>
            <br>  <br>  <br>  <br>  <br>
            <img src="<?= base_url(); ?>assets6/images/headernew2.png" class="banner-mock" alt="Awesome Image" />
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="content-block">
                            <br><br><br><br><br>
                           <p><a href="<?= base_url(); ?>smartcity/tentang"  target="_blank" class="button">Tentang Kota Malang Smartcity</a></p>
                            <br><br>
                           <div class="row no-gutter" style="text-align: center;">
                            <div class="col-sm-4" style="margin-top:30px;">
                               <a href="<?= base_url(); ?>smartcity/smart_economy" class="banner-btn "  target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/smart_economy.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br> Economy</span>
                                </a>
                            </div>
                             <div class="col-sm-4" style="margin-top:30px;">
                               <a href="<?= base_url(); ?>smartcity/smart_governance" class="banner-btn "  target="_blank">
                                   <img src="<?= base_url(); ?>assets/images/icon_new/smart_goverment.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br> Governance</span>
                                </a>
                            </div>
                             <div class="col-sm-4" style="margin-top:30px;">
                               <a href="<?= base_url(); ?>smartcity/smart_branding" class="banner-btn "  target="_blank">
                                    <img src="<?= base_url(); ?>assets/images/icon_new/smart_brending.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br> Branding</span>
                                </a>
                            </div>
                            <div class="col-sm-4" style="margin-top:30px;">
                               <a href="https://covid19.malangkota.go.id/beranda" class="banner-btn"  target="_blank">
                                <img src="<?= base_url(); ?>assets/images/icon_new/smart_living.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br>Living</span>
                                </a>
                            </div>
                             <div class="col-sm-4" style="margin-top:30px;">
                               <a href="<?= base_url(); ?>smartcity/smart_living" class="banner-btn "  target="_blank">
                                    <img src="<?= base_url(); ?>assets/images/icon_new/smart_society.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br> Society</span>
                                </a>
                            </div>
                             <div class="col-sm-4" style="margin-top:30px;">
                               <a href="<?= base_url(); ?>smartcity/smart_society" class="banner-btn "  target="_blank">
                                   <img src="<?= base_url(); ?>assets/images/icon_new/smart_environment.png" style="height: 80px; width: 80px" class="main-logo"  width="" alt="Awesome Image" />
                                    <span>Smart<br> Environment</span>
                                </a>
                            </div>
                        </div>

                        </div><!-- /.content-block -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.banner-style-one -->





                    <img src="<?= base_url(); ?>assets/images/iconic_mlg_nonbg.png" width="100%">

            <hr class="rgba-white-light" style="margin: 0 15%;">
       <div class=" d-flex text-center justify-content-center mb-md-0 mb-4">
            </div>

                <div class="col-md-12">

                       <div class="footer-widget">

                                <div class="social-block text-center ">
                                      <a href="https://www.facebook.com/malangkota.go.id" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/facebook.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-twitter --></a>
                                      <a href="https://twitter.com/PemkotMalang" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/twitter.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-twitter --></a>
                                      <a href="https://www.instagram.com/pemkotmalang/" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/instagram.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-instagram --></a>
                                      <a href="https://www.youtube.com/user/mediacentermalang" target="_blank"><i><img src="<?= base_url(); ?>assets6/images/youtube.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i><!-- /.fab fa-instagram --></a>
                                </div><!-- /.social-block -->
                            </div><!-- /.footer-widget -->
                </div>

        </div>
        <div class="bottom-footer text-center">
            <div class="container">
                <p>&copy; copyright 2021 <a href="#" style="color: blue">Pemerintah Kota Malang</a></p>
            </div><!-- /.container -->
        </div><!-- /.bottom-footer -->
    </div><!-- /.page-wrapper -->
    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i><img src="<?= base_url(); ?>assets6/images/icon_portal/up-chevron.png" style="height: 30%; width: 30%" class="main-logo"  width="" alt="Awesome Image" /></i></a>
    <!-- /.scroll-to-top -->
    <script src="<?= base_url(); ?>assets6/js/jquery.js"></script>
    <script src="<?= base_url(); ?>assets6/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.bxslider.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/wow.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.validate.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>assets6/js/theme.js"></script>
</body>
</html>


<!-- ===================PORTAL MALANGKOTA ================================-->
<!-- ========================= DONAT ================================-->

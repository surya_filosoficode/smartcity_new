<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel extends CI_Model{
    
    public function get_data_all($table){
    	$data = $this->db->get($table);
    	return $data->result();
    }

    public function get_data_all_where($table, $where){
    	$data = $this->db->get_where($table, $where);
    	return $data->result();
    }

    public function get_data_each($table, $where){
    	$data = $this->db->get_where($table, $where);
    	return $data->row_array();
    }

    public function insert_data($table, $data){
    	$insert = $this->db->insert($table, $data);
    	return $insert;
    }

    public function update_data($table, $set, $where){
    	$update = $this->db->update($table, $set, $where);
    	return $update;
    }

    public function delete_data($table, $where){
    	$delete = $this->db->delete($table, $where);
    	return $delete;
    }


    
    

}
?>
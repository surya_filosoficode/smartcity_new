 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_v0 {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================

        public function set_session($param = null){

            // print_r($param);
        	$status = false;
            if($param != null){
        		$this->session->set_userdata("ih_mau_ngapain", $param);
                $status = true;
        	}
            return $status;
        }

        public function destroy_session($redirect = "login"){
            $this->session->sess_destroy();
            redirect($redirect);
        }

        public function get_session(){
            $data_session = array();
            if(isset($_SESSION["ih_mau_ngapain"])){
                $data_session = $_SESSION["ih_mau_ngapain"];
            }
            return $data_session;
        }

        public function auth_login(){
            if(isset($_SESSION["ih_mau_ngapain"])){
                if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["ih_mau_ngapain"]["priviledge"]) {
                        case "0":
                            redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            redirect(base_url()."cs/home");
                            break;
                        

                        default:
                            // redirect(base_url()."login");
                            break;
                    }
                }
            }
        }

        public function check_session_active_ad(){
            if(isset($_SESSION["ih_mau_ngapain"])){
                if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["ih_mau_ngapain"]["id_tipe_admin"]) {
                        case "0":
                            // redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            redirect(base_url()."cs/home");
                            break;
                        

                        default:
                            redirect(base_url()."login");
                            break;
                    }
                }else {
                    redirect(base_url()."login");
                }
            }else {
                redirect(base_url()."login");
            }
        }

        public function check_session_active_fl(){
            if(isset($_SESSION["ih_mau_ngapain"])){
                if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["ih_mau_ngapain"]["id_tipe_admin"]) {
                        case "0":
                            redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            // redirect(base_url()."fl/home_main");
                            break;
                        

                        default:
                            redirect(base_url()."login");
                            break;
                    }
                }else {
                    redirect(base_url()."login");
                }
            }else {
                redirect(base_url()."login");
            }
        }
    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================

    // ===========================================================================
    // ----------------------------------User_Login-------------------------------
    // ===========================================================================

        public function set_session_for_user($param = null){
            $status = false;
            if($param != null){
                $this->session->set_userdata("idih_cari_apa_tong", $param);
                $status = true;
            }
            return $status;
        }

        public function destroy_session_for_user($redirect = "login"){
            $this->session->sess_destroy();
            redirect($redirect);
        }

        public function get_session_for_user(){
            $data_session = array();
            if(isset($_SESSION["idih_cari_apa_tong"])){
                $data_session = $_SESSION["idih_cari_apa_tong"];
            }
            return $data_session;
        }

        public function auth_login_for_user(){
            if(isset($_SESSION["idih_cari_apa_tong"])){
                if($_SESSION["idih_cari_apa_tong"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["idih_cari_apa_tong"]["id_tipe_user"]) {
                        case "0":
                            redirect(base_url()."user/count/home");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            redirect(base_url()."frontliner/data_item");
                            break;
                        

                        default:
                            // redirect(base_url()."login");
                            break;
                    }
                }
            }
        }

        public function check_session_active_for_user(){
            if(isset($_SESSION["idih_cari_apa_tong"])){
                if($_SESSION["idih_cari_apa_tong"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["idih_cari_apa_tong"]["id_tipe_user"]) {
                        case "0":
                            // redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            redirect(base_url()."frontliner/data_item");
                            break;
                        

                        default:
                            redirect(base_url()."user/login");
                            break;
                    }
                }else {
                    redirect(base_url()."user/login");
                }
            }else {
                redirect(base_url()."user/login");
            }
        }

        public function check_session_active_fl_for_user(){
            if(isset($_SESSION["idih_cari_apa_tong"])){
                if($_SESSION["idih_cari_apa_tong"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["idih_cari_apa_tong"]["id_tipe_user"]) {
                        case "0":
                            redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                        
                        case "1":
                            // print_r("fl");
                            // redirect(base_url()."fl/home_main");
                            break;
                        

                        default:
                            redirect(base_url()."login");
                            break;
                    }
                }else {
                    redirect(base_url()."login");
                }
            }else {
                redirect(base_url()."login");
            }
        }
    // ===========================================================================
    // ----------------------------------User_Login-------------------------------
    // ===========================================================================

}
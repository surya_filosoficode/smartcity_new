<?php
class Smartcity extends CI_Controller
{
    public $title;
    public $seo_description;
    public $seo_keywords;

    public $tbl_main;
    public $tbl_smart_cate;

    function __construct(){
        parent::__construct();
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");

        $this->title = "5AM SMART CITY";
        $this->seo_description = "Website Prortal Malang Smart City, Malang Kota Cerdas, Malang Smartcity";
        $this->seo_keywords = "malangkota.go.id, portal malang smartcity, portal kota malang";
        
        $this->tbl_main = "mn_smart";
        $this->tbl_smart_cate = "mn_smart_ct";
    }

    function index(){
        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["list_data"] = [];

        $this->db->order_by("row_mn_smart_ct", "ASC");
        $cate = $this->mm->get_data_all_where(
            $this->tbl_smart_cate,
            ["id_mn_smart"=>"0"]
        );

        foreach ($cate as $key => $value) {
            $id_mn_smart_ct = $value->id_mn_smart_ct;
            
            $this->db->order_by("row_mn_smart", "ASC");
            $data_menu = $this->mm->get_data_all_where(
                $this->tbl_main,
                ["id_mn_smart_ct"=>$id_mn_smart_ct]
            );

            $data["list_data"][$id_mn_smart_ct]["title"] = $value;
            $data["list_data"][$id_mn_smart_ct]["item"] = $data_menu;
        }

        // print_r("<pre>");
        // print_r($data["list_data"]);
        // print_r("<pre>");
        // die();

		$this->load->view('index', $data);
    }

    function index_sub($id_parent){

        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["patern"] = [];

        $data_perent = $this->mm->get_data_each($this->tbl_main, ["sha2(id_mn_smart, '256') = "=>$id_parent]);
        $data["parent"] = $data_perent;
        if($id_parent != "0"){
            $pattern = $data_perent["parent_mn_smart"];
            
            $data["seo_keywords"] = $data_perent["seo_mn_smart"];
            $arr_parent = json_decode($pattern);
            
            foreach ($arr_parent as $key => $value) {
                $tmp_val = $this->mm->get_data_each($this->tbl_main, ["id_mn_smart"=>$value]);
                $data["patern"][$key] = $tmp_val;   
            }
        }

        // $data["list_data"] = $this->mm->get_data_all_where(
        //     $this->tbl_main,
        //     ["sha2(mn_smart_id, '256') = "=>$id_parent, "sts_active"=>"1"]
        // );

        $data["list_data"] = [];

        $this->db->order_by("row_mn_smart_ct", "ASC");
        $cate = $this->mm->get_data_all_where(
            $this->tbl_smart_cate,
            ["sha2(id_mn_smart, '256') = "=>$id_parent]
        );

        foreach ($cate as $key => $value) {
            $id_mn_smart_ct = $value->id_mn_smart_ct;
            
            $this->db->order_by("row_mn_smart", "ASC");
            $data_menu = $this->mm->get_data_all_where(
                $this->tbl_main,
                ["id_mn_smart_ct"=>$id_mn_smart_ct]
            );

            $data["list_data"][$id_mn_smart_ct]["title"] = $value;
            $data["list_data"][$id_mn_smart_ct]["item"] = $data_menu;
        }

        
        // print_r("<pre>");
        // print_r($data);
        // die();
		$this->load->view('index_sub', $data);
    }

    function test(){
        $data["page"] = "p_user_profil";
        $data["title"] = $this->title;
        $data["seo_description"] = $this->seo_description;
        $data["seo_keywords"] = $this->seo_keywords;

        $data["list_data"] = [];

        $this->db->order_by("row_mn_smart_ct", "ASC");
        $cate = $this->mm->get_data_all_where(
            $this->tbl_smart_cate,
            ["id_mn_smart"=>"0"]
        );

        foreach ($cate as $key => $value) {
            $id_mn_smart_ct = $value->id_mn_smart_ct;
            
            $this->db->order_by("row_mn_smart", "ASC");
            $data_menu = $this->mm->get_data_all_where(
                $this->tbl_main,
                ["id_mn_smart_ct"=>$id_mn_smart_ct]
            );

            $data["list_data"][$id_mn_smart_ct]["title"] = $value;
            $data["list_data"][$id_mn_smart_ct]["item"] = $data_menu;
        }

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("test", $data);
    }

    function tentang(){
        $this->load->view("tentang");
    }
}
